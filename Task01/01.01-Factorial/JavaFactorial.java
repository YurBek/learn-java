// Практикум 1. Задачи начального уровня:
// 1. Рекурсивно посчитать факториал (n!) числа n.

package javafactorial;

import java.util.Scanner;

public class JavaFactorial
{
    // функция для нахождения факториала
    static long Factorial(long num){
        if(num == 0) // факториал 0 равен 1
            return 1;
        else
            return num * Factorial(num - 1); // рекурсивный вызов функции
    }
    
    public static void main(String[] args){
        
        Scanner scan = new Scanner(System.in);
        long factorial = 0;
        
        do{
            // просим ввести число
            System.out.print("Введите целое положительное число: ");
        
            if(!scan.hasNextInt()){
                // проверка: целое число или нет
                System.out.println("\nВы ввели не целое число!!! -> " + scan.next());
            }
            else{
                int numInt = scan.nextInt();
                if(numInt < 0)
                    // проверка: положительное число или нет
                    System.out.println("\nВы ввели отрицательное число!!! -> " + numInt);
                else if(numInt > 20)
                    // факториал 20 это предел для целого типа long
                    System.out.println("\nВы ввели слишком большое число!!! -> " + numInt);
                else{
                    // вычисляем факториал
                    factorial = Factorial(((long)numInt));
                    System.out.println("\nФакториал (n!) числа " + numInt + " равен: " + factorial);
                }
            }
        }while(factorial == 0);
    }
}
