// Практикум 1. Задачи начального уровня:
// 2. Из строкового представления числа в двоичной системе исчисления
// получить десятичную запись.

/* Используем метод Горнера: для преобразования числа из двоичной в десятичную
систему, надо суммировать цифры слева направо, умножая ранее полученный
результат на 2. */

package binarytodecimal;

import java.util.Scanner;

public class BinaryToDecimal {
    
    static long fBinaryConvert(String sBinary){
        long decimal = 0;
        int size = sBinary.length();
        // Преобразовываем в десятичное
        for(int count = 0; count < size; ++count){
            if(sBinary.charAt(count) == '1')
                decimal = 2 * decimal + 1;
            else
                decimal = 2 * decimal + 0;
        }
        return decimal;
    }
    
    public static void main(String[] args) {
        // Вводим двоичное число 101010111
        System.out.print("Введите двоичное число: ");
        Scanner scan = new Scanner(System.in);
        String sBinary = scan.nextLine();
        long decimal = fBinaryConvert(sBinary);
        System.out.println("\nВы ввели двоичное число: " + sBinary);
        System.out.println("оно соотвествует десятичному числу: " + decimal);
    }
}
