// Практикум 1. Задачи начального уровня:
// 3. Из десятичной записи числа получить двоичную, восьмиричную.

package decimaltobinaryandoctal;

import static java.lang.Math.abs;
import java.util.Scanner;

public class DecimalToBinaryAndOctal {
    
    public static void main(String[] args) {
        
        System.out.println("Сделаем преобразование десятичного числа в двоичную запись и в восьмеричную.");
        System.out.print("Введите число: ");
        Scanner scan = new Scanner(System.in);
        if(!scan.hasNextDouble())
            // проверка: если ввели не число
            System.out.println("\nВы ввели не число!");
        else{
            double decimal = scan.nextDouble();
            System.out.println("\nЧисло " + decimal);

            // преобразование в двоичную запись
            String sBinary = fDecimalConvert(decimal, 2);
            System.out.println("в двоичной записи " + sBinary);
            
            // преобразование в восьмиричную запись
            String sOctal = fDecimalConvert(decimal, 8);
            System.out.println("и в восьмеричной записи " + sOctal);
            }
    }
    
    // общая функция для преобразования из десятичной записи
    // в двоичную и восьмеричную системы счисления
    // в функцию передается само число (decimal) и система счисления (numNotation)
    static String fDecimalConvert(double decimal, int numNotation){
        String sDigit;
        if (decimal == 0){
            // если введеное число равно 0
            sDigit = "0.00000000";
        }
        else {
            double unsignDecimal = abs(decimal); // выделяем модуль числа
            long decimalInteger = (long)unsignDecimal; // выделяем целую часть числа
            // преобразовываем целую часть числа
            sDigit = fIntegerConvert(decimalInteger, numNotation);
            
            // выделяем дробную часть числа
            double decimalFractional = unsignDecimal - (double)decimalInteger;
            if(decimalFractional == 0){
                // если дробная часть равна 0
                sDigit = sDigit + ".00000000";
            }
            else {
                // преобразовываем дробную часть числа
                sDigit = sDigit + "." + fFractionalConvert(decimalFractional, numNotation);
            }
        }
        if (decimal < 0){
            // если число отрицательное добавляем знак минус
            sDigit = "-" + sDigit;
        }
        
        return sDigit;
    }
    
    // функция преобразования целой части чила
    // в двоичную и восьмеричную системы счисления
    // в функцию передается целая часть числа (decimal) и система счисления (numNotation)
    static String fIntegerConvert(long decimalInteger, int numNotation){
        String sInteger = "";
        do {
            // делим исходное число (decimalInteger) на показатель системы счисления (numNotation)
            // остаток от деления дает нам цифру разряда нового числа
            // первая цифра будет младшим разрядом, а последняя - старшим
            sInteger = (decimalInteger % ((long)numNotation)) + sInteger;
            decimalInteger = decimalInteger / ((long)numNotation);
            
            // все поторяем пока целочисленное деление не даст 0
        } while (decimalInteger > 0);
        return sInteger;
    }
    
    // функция преобразования целой части чила
    // в двоичную и восьмеричную системы счисления
    // в функцию передается дробная часть числа (decimal) и система счисления (numNotation)
    static String fFractionalConvert(double decimalFractional, int numNotation){
        String sFractional = "";
        // при преобразовании дробной части
        // разрядность ограничим восью знаками (digit < 8)
        for (int digit=0; digit < 8; ++digit) {
            // умножаем дробную часть (decimalFractional) на показатель системы счисления (numNotation)
            double dDFnN = decimalFractional * (double)numNotation;
            // целую часть результата используем как разряд нового числа
            sFractional = sFractional + (long)dDFnN;
            decimalFractional = dDFnN - (long)dDFnN;
        }
        return sFractional;
    }
}
